CREATE TABLE IF NOT EXISTS avg_btc_price
(
    avgPriceUsd numeric,
    priceFrom   timestamptz,
    priceTo     timestamptz
);

insert into avg_btc_price (avgPriceUsd, priceFrom, priceTo)
    (select AVG(priceusd) as avg_price, NOW() - INTERVAL '15 minutes' as priceFrom, NOW() as priceTo
     from btc_price
     where processedtimestamp >= NOW() - INTERVAL '15 minutes');
