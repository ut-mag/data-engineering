CREATE TABLE IF NOT EXISTS processed_orders
(
    id                 serial PRIMARY KEY,
    orderData          jsonb,
    processedTimestamp timestamptz
);

INSERT INTO processed_orders (orderData, processedTimestamp)
VALUES ('{{ti.xcom_pull(key='processed_order')}}', NOW());
