CREATE TABLE IF NOT EXISTS btc_price
(
    priceUsd           numeric,
    processedTimestamp timestamptz
);

INSERT INTO btc_price(priceUsd, processedTimestamp)
VALUES ({{ti.xcom_pull(task_ids='fetch_btc_price', key='btc_price')}}, TO_TIMESTAMP({{ ti.xcom_pull(task_ids='fetch_btc_price', key='btc_price_timestamp') }} / 1000));
