import json
import os
from datetime import datetime, timedelta

import requests
from airflow import DAG
from airflow.operators.bash import BashOperator
from airflow.operators.python import PythonOperator
from airflow.providers.postgres.operators.postgres import PostgresOperator
from airflow.sensors.filesystem import FileSensor
from airflow.models import Variable

DEFAULT_ARGS = {
  'owner': 'Tartu',
  'depends_on_past': False,
  'retries': 0
}

ORDER_API_URL = Variable.get("ORDER_API_URL")
API_AUTH = Variable.get("API_AUTH")

DATA_FOLDER = '/tmp/data'
ORDERS_FOLDER = os.path.join(DATA_FOLDER, 'orders')
FILE_NAMES = {
  'buy_order': 'buy_order.json',
  'sell_order': 'sell_order.json',
}

order_trigger = DAG(
  dag_id='order_trigger',
  schedule_interval="* * * * *",
  start_date=datetime(2022, 9, 14, 9, 15, 0),
  catchup=False,
  template_searchpath=DATA_FOLDER,
  default_args=DEFAULT_ARGS,
  concurrency=1,
  max_active_runs=1,
)


def send_orders(**kwargs):
  if os.path.exists(ORDERS_FOLDER) and os.path.isdir(ORDERS_FOLDER):
    order_json_files = os.listdir(ORDERS_FOLDER)
    for file_name in order_json_files:
      with open(os.path.join(ORDERS_FOLDER, file_name), 'r') as f:
        order = f.read()
        print(f"Sending order {order}")
        send_order(order, file_name.split('.')[0], **kwargs)


def send_order(order, order_type, **kwargs):
  response = requests.post(ORDER_API_URL, json=json.loads(order), headers={'Auth': API_AUTH})
  print(response)
  if response.status_code == 200:
    print(f"Order sent successful")
    kwargs['ti'].xcom_push(key='processed_order', value=order)
    kwargs['ti'].xcom_push(key='processed_order_file', value=FILE_NAMES[order_type])
  else:
    raise Exception(f"Request failed with status code {response.status_code} and message {response.text}")


order_json_sensor = FileSensor(
  task_id="order_json_sensor",
  fs_conn_id="data_folder_connection",
  filepath=ORDERS_FOLDER,
  mode='poke',
  poke_interval=timedelta(seconds=10),
  dag=order_trigger
)

send_orders = PythonOperator(
  task_id='send_orders',
  dag=order_trigger,
  python_callable=send_orders,
  retries=3,
  retry_delay=timedelta(seconds=5)
)

save_order = PostgresOperator(
  task_id='save_order',
  dag=order_trigger,
  postgres_conn_id='airflow_pg',
  sql='insertProcessedOrder.sql',
  trigger_rule='one_success',
  autocommit=True
)

delete_order_json = BashOperator(
  task_id='delete_order_json',
  dag=order_trigger,
  trigger_rule='none_failed',
  bash_command='echo $file_name; rm -f ' + ORDERS_FOLDER + '/$file_name',
  env={"file_name": '{{ ti.xcom_pull(key="processed_order_file")}}'}
)

order_json_sensor >> send_orders >> save_order >> delete_order_json
