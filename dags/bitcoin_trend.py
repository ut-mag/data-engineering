import json
from datetime import datetime, timedelta

import requests
from airflow import DAG
from airflow.operators.python import PythonOperator, BranchPythonOperator
from airflow.providers.postgres.operators.postgres import PostgresOperator

DEFAULT_ARGS = {
  'owner': 'Tartu',
  'depends_on_past': False,
  'retries': 1,
  'retry_delay': timedelta(minutes=5)
}

BTC_API_URL = 'https://api.coincap.io/v2/assets'

DATA_FOLDER = '/tmp/data'
ORDERS_FOLDER = DATA_FOLDER + '/orders'

btc_trend = DAG(
  dag_id='btc_trend',
  schedule_interval='* * * * *',  # execute every minute
  start_date=datetime(2022, 9, 14, 9, 15, 0),
  catchup=False,
  template_searchpath=DATA_FOLDER,
  default_args=DEFAULT_ARGS,
)


def fetch_btc_price(url, **kwargs):
  r = requests.get(url)
  r_json = r.json()
  unix_ts = r_json['timestamp']
  price_usd = [prices['priceUsd'] for prices in r_json['data'] if prices['id'] == 'bitcoin'][0]
  kwargs['ti'].xcom_push(key="btc_price", value=price_usd)
  kwargs['ti'].xcom_push(key="btc_price_timestamp", value=unix_ts)


def trigger_orders(**kwargs):
  avg_price = kwargs['ti'].xcom_pull(task_ids='get_average_btc_price')[0][0]
  last_prices = [price[0] for price in kwargs['ti'].xcom_pull(task_ids='get_last_btc_prices')]
  last_price = last_prices[0]
  print(f"Average price {avg_price}")
  print(f"Last prices {last_prices}")

  order = {
    'currentPrice': float(last_price),
    'rollingAveragePrice': float(avg_price)
  }

  if last_price > avg_price and all(price < avg_price for price in last_prices[1:]):
    order['orderType'] = 'Buy'
    kwargs['ti'].xcom_push(key='buy_order', value=order)
    return 'buy_order'
  if last_price < avg_price and all(price > avg_price for price in last_prices[1:]):
    order['orderType'] = 'Sell'
    kwargs['ti'].xcom_push(key='sell_order', value=order)
    return 'sell_order'


def save_order(output_folder, xcom_key, file_name, **kwargs):
  order = kwargs['ti'].xcom_pull(key=xcom_key)
  with open(f'{output_folder}/{ORDERS_FOLDER}/{file_name}', 'w') as f:
    json.dump(order, f)


fetch_btc_price = PythonOperator(
  task_id='fetch_btc_price',
  dag=btc_trend,
  trigger_rule='none_failed',
  python_callable=fetch_btc_price,
  op_kwargs={
    'url': BTC_API_URL
  }
)

insert_btc_price = PostgresOperator(
  task_id='insert_btc_price',
  dag=btc_trend,
  postgres_conn_id='airflow_pg',
  sql='insertBtc.sql',
  trigger_rule='none_failed',
  autocommit=True
)

fetch_btc_price >> insert_btc_price

insert_avg_btc_price = PostgresOperator(
  task_id='insert_avg_btc_price',
  dag=btc_trend,
  postgres_conn_id='airflow_pg',
  sql='insertAvgBtcPrice.sql',
  trigger_rule='none_failed',
  autocommit=True
)

insert_btc_price >> insert_avg_btc_price

get_average_btc_price = PostgresOperator(
  task_id='get_average_btc_price',
  dag=btc_trend,
  postgres_conn_id='airflow_pg',
  sql='selectLatestAvgBtcPrice.sql',
  trigger_rule='none_failed',
  autocommit=True
)

get_last_btc_prices = PostgresOperator(
  task_id='get_last_btc_prices',
  dag=btc_trend,
  postgres_conn_id='airflow_pg',
  sql='selectLastBtcPrices.sql',
  trigger_rule='none_failed',
  autocommit=True
)

trigger_orders = BranchPythonOperator(
  task_id='trigger_orders',
  dag=btc_trend,
  trigger_rule='none_failed',
  python_callable=trigger_orders
)

insert_avg_btc_price >> (get_average_btc_price, get_last_btc_prices) >> trigger_orders

buy_order = PythonOperator(
  task_id='buy_order',
  dag=btc_trend,
  trigger_rule='none_failed',
  python_callable=save_order,
  op_kwargs={
    'output_folder': DATA_FOLDER,
    'xcom_key': 'buy_order',
    'file_name': 'buy_order.json'
  }
)

sell_order = PythonOperator(
  task_id='sell_order',
  dag=btc_trend,
  trigger_rule='none_failed',
  python_callable=save_order,
  op_kwargs={
    'output_folder': DATA_FOLDER,
    'xcom_key': 'sell_order',
    'file_name': 'sell_order.json'
  }
)

trigger_orders >> [buy_order, sell_order]
